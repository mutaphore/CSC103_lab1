import java.util.*;

public class ListWork
{

   public static <T> boolean search (T [] arr, T target)
   {  
      boolean found = false;
      
      for(int i=0 ; i<arr.length ; i++)
      {
         if(arr[i].equals(target))
         {
            found = true;      
            return found;       
         }
      }
      return found;
   }
   
   public static <T> void print (T [] arr)
   {
      for(int i=0 ; i<arr.length ; i++)
      {
         System.out.println(arr[i]);
      }
   }

   public static void main(String[] args)
   {
   
      Integer[] array = new Integer[10];
      Scanner input = new Scanner(System.in);
      int i = 0;
      String search;
   
      System.out.println("Please enter 10 integers: ");
      
      while(i < 10)
      {
         try
         {
            array[i] = input.nextInt();
            i++;
         }
         catch(InputMismatchException e)
         {
            input.next();
         }
         
      }
      
      input.nextLine();

      System.out.print("Do you want to search for a item (y to continue): ");
      search = input.next();
      
      while(search.equals("y") || search.equals("Y"))
      {
         
         System.out.println("Enter target: "); 
         
         try
         {
            if(search(array,input.nextInt()))
               System.out.println("Target found!");
            else
               System.out.println("Target not found!"); 
         }
         catch(InputMismatchException e)
         {
            System.out.println("Not an integer!"); 
            input.next();
         } 
         
         System.out.print("Do you want to search for a item (y to continue): ");
         search = input.next(); 
    
      }
      
      print(array);
            
   }
         
}



